package com.bookpurohit.tests.login;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.bookpurohit.common.Common;
import com.bookpurohit.common.ExcelReader;

public class LoginLogOut extends Common {

	// Verify that the admin User is able to Login with Valid Credentials

	@BeforeClass
	public void init() throws IOException {
		loadFiles();
	}

	@BeforeMethod
	public void openBrowser() {
		initBrowsers();
	}

	@Test(priority = 2, dependsOnMethods = "verifyPageTitle", enabled = true)
	public void verifyLoginLogout() throws InterruptedException {

		Thread.sleep(1500);

		// 3.Enter valid user name
		driver.findElement(By.xpath("//input[@id='formly_2_input_email_0']")).sendKeys("manidhar@bookpurohit.net");
		// 4. Enter valid Password
		driver.findElement(By.id("formly_2_input_password_1")).sendKeys("Manidhar@123");
		// 5. Click on Submit button
		// driver.findElement(By.xpath("//button[contains(text(),'Submit')]")).click();
		driver.findElement(By.id("formly_2_input_password_1")).sendKeys(Keys.ENTER);
		log.debug("Logging in to application with valid credentials");
		Thread.sleep(1500);
		// 6. Verify that User should be navigated to Dashboard page
		String actualEmailId = driver.findElement(By.xpath("//strong[contains(text(),'@bookpurohit.net')]")).getText()
				.toLowerCase();

		String expectedEmailId = "manidhar@bookpurohit.net";

		log.debug(actualEmailId);
		log.debug(expectedEmailId);

		Assert.assertEquals(actualEmailId, expectedEmailId);
		log.debug("Validating the email id on dashboard");
		// 7. Click on Logout
		driver.findElement(By.xpath("//strong[contains(text(),'@bookpurohit.net')]")).click();
		driver.findElement(By.linkText("Logout")).click();
		log.debug("Logged out of application");
	}

	@Test(priority = 1)
	public void verifyPageTitle() throws InterruptedException {
		Thread.sleep(1500);
		SoftAssert softassert = new SoftAssert();

		String actualPageTitle = driver.getTitle();
		String expectedPageTitle = "Purohit Admin";
		// Assert.assertEquals(actualPageTitle, expectedPageTitle);
		softassert.assertEquals(actualPageTitle, expectedPageTitle);
		log.debug("verified the page title of login page");
		// softassert.assertTrue(false, "assert true is not matching");
		// boolean flag=actualPageTitle.equals(expectedPageTitle);
		// Assert.assertTrue(flag);
		// Assert.assertTrue(flag,"Titles are not matching, check the flow manually,
		// actual is: "+actualPageTitle+ " and expected is : "+expectedPageTitle);

		// Assert.assertFalse(flag, "Failing the test case as title should not be
		// 'Purohit Admin' ");
		// Assert.assertNull(null);

		softassert.assertAll();
	}

	@Test(priority = 3, enabled = true, dataProvider = "data")
	public void verifyInvalidLogin(String username, String password) throws InterruptedException, IOException {
		try {
			Thread.sleep(1500);

			// 3.Enter valid user name
			driver.findElement(By.xpath("//input[@id='formly_2_input_email_0']")).sendKeys(username);
			// 4. Enter valid Password
			driver.findElement(By.id("formly_2_input_password_1")).sendKeys(password);
			log.debug("Trying to login with credentials: " + username + " and " + password);
			// 5. Click on Submit button
			driver.findElement(By.id("formly_2_input_password_1")).sendKeys(Keys.ENTER);
			Thread.sleep(1500);
			// 6. Verify that Error message
			String actualErrorMessage = driver.findElement(By.xpath("//div[@role='alert5555']")).getText();

			String expectedErrorMessage = "Invalid Login & Password";

			log.debug(actualErrorMessage);
			log.debug(expectedErrorMessage);

			Assert.assertEquals(actualErrorMessage, expectedErrorMessage);
			log.debug("Validated the error message");
		} catch (Exception e) {
			// capture the screenshot
			captureScreenshot();
			throw e;
		}

	}

	@DataProvider
	public Object[][] data() throws EncryptedDocumentException, IOException {
		ExcelReader excel = new ExcelReader();
		return excel.readExcel();
	}

	@AfterMethod
	public void closeBrowser() {
		closeBrowsers();
	}

}
