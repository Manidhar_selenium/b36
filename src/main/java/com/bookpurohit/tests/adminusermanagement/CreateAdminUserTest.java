package com.bookpurohit.tests.adminusermanagement;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.bookpurohit.common.Common;

public class CreateAdminUserTest extends Common{

	// Verify that the admin User is able to Login with Valid Credentials

	@BeforeMethod
	public void openBrowser() {
		initBrowsers();
	}

	@Test(priority = 2,dependsOnMethods = "verifyPageTitle",enabled=true)
	public void verifyLoginLogout() throws InterruptedException {

		Thread.sleep(1500);

		// 3.Enter valid user name
		driver.findElement(By.xpath("//input[@id='formly_2_input_email_0']")).sendKeys("manidhar@bookpurohit.net");
		// 4. Enter valid Password
		driver.findElement(By.id("formly_2_input_password_1")).sendKeys("Manidhar@123");
		// 5. Click on Submit button
		//driver.findElement(By.xpath("//button[contains(text(),'Submit')]")).click();
		driver.findElement(By.id("formly_2_input_password_1")).sendKeys(Keys.ENTER);
		Thread.sleep(1500);
		// 6. Verify that User should be navigated to Dashboard page
		String actualEmailId = driver.findElement(By.xpath("//strong[contains(text(),'@bookpurohit.net')]")).getText()
				.toLowerCase();

		String expectedEmailId = "manidhar@bookpurohit.net";

		System.out.println(actualEmailId);
		System.out.println(expectedEmailId);
		
		Assert.assertEquals(actualEmailId, expectedEmailId);
		// 7. Click on Logout
		driver.findElement(By.xpath("//strong[contains(text(),'@bookpurohit.net')]")).click();
		driver.findElement(By.linkText("Logout")).click();
	}
	@Test(priority = 1)
	public void verifyPageTitle() throws InterruptedException{
		Thread.sleep(1500);

		String actualPageTitle=driver.getTitle();
		String expectedPageTitle="Purohit Admin";
		Assert.assertEquals(actualPageTitle, expectedPageTitle);
	}

	@AfterMethod
	public void closeBrowser() {
		closeBrowsers();
	}

}
