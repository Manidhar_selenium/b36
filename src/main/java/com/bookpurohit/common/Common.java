package com.bookpurohit.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Common {
	public Properties env = null;
	public WebDriver driver = null;
	public Logger log = null;
	
	public String path=System.getProperty("user.dir");

	public void loadFiles() throws IOException {
		InputStream file = new FileInputStream(
				path+"\\src\\main\\resources\\EnvironmentDetails.properties");
		env = new Properties();
		env.load(file);
		System.out.println("Property file loaded");

		log = Logger.getLogger("rootLogger");
		log.debug("Logger configured successfully");
	}

	public void initBrowsers() {
		// 1. open the chrome Browser
		System.setProperty("webdriver.chrome.driver",
				path+"\\src\\main\\resources\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		// 2. Naviagate to Bookpurohit admin app login page
		driver.get(env.getProperty("url"));
		log.debug("Chrome Browser opened");

	}

	public void closeBrowsers() {
		// 8. Close the browser
		driver.close();
		log.debug("Closing the Browser");

	}
	public void captureScreenshot() throws IOException{
		//capture screenshot code here...
		TakesScreenshot screenshot=((TakesScreenshot)driver);
		File srcFile=screenshot.getScreenshotAs(OutputType.FILE);
		File destFile=new File(path+"\\src\\main\\resources\\screenshots\\Screenshot.png");
		FileUtils.copyFile(srcFile, destFile);
	}
}
